package model;

public class PoisonousMushroom extends Mushroom {

    @Override
    public char getAppearance() {
        return '+';
    }

    @Override
    public boolean isPoisonous() {
        return true;
    }
}
