package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MushroomListGenerator {
    private int width;
    private int length;
    private static Random random = new Random();
    private List<Mushroom> mushrooms = new ArrayList();

    public MushroomListGenerator(int length, int width) {
        this.width = width;
        this.length = length;
    }

    public List<Mushroom> getMushrooms(int numberOfMushrooms, int numberOfPoisons) {
        fillList(numberOfMushrooms, numberOfPoisons);
        return mushrooms;
    }

    private void fillList(int numberOfMushrooms, int numberOfPoisons) {
        for (int i = 0; i < numberOfMushrooms; i++) {
            addMushroomAtRandomPosition(new Mushroom());
        }
        for (int i = 0; i < numberOfPoisons; i++) {
            addMushroomAtRandomPosition(new PoisonousMushroom());
        }
    }

    private void addMushroomAtRandomPosition(Mushroom mushroom) {
        int x = random.nextInt(width);
        int y = random.nextInt(length);
        for (Mushroom m : mushrooms) {
            if ((x == m.getXPoint()) && (y == m.getYPoint())) {
                x = random.nextInt(width);
                y = random.nextInt(length);
            }
        }
        mushroom.setYPoint(y);
        mushroom.setXPoint(x);
        mushrooms.add(mushroom);
    }
}
