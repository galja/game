package model;

public class Pig extends Figure {
    private boolean isAlive;

    public Pig(int xStartPoint, int yStartPoint) {
        // setAppearance(appearance);
        setXPoint(xStartPoint);
        setYPoint(yStartPoint);
    }

    @Override
    public char getAppearance() {
        return '@';
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public void goStraight() {
        this.setYPoint(getYPoint() - 1);
    }

    public void goBack() {
        this.setYPoint(getYPoint() + 1);
    }

    public void goLeft() {
        this.setXPoint(getXPoint() - 1);
    }

    public void goRight() {
        this.setXPoint(getXPoint() + 1);
    }
}
