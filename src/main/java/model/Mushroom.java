package model;

public class Mushroom extends Figure {
    public boolean isPoisonous() {
        return false;
    }

    @Override
    public char getAppearance() {
        return '*';
    }
}
