package model;

public class Figure {
    private char appearance;
    private int xPoint;
    private int yPoint;

    public char getAppearance() {
        return appearance;
    }

    public void setAppearance(char appearance) {
        this.appearance = appearance;
    }

    public int getXPoint() {
        return xPoint;
    }

    public void setXPoint(int xPoint) {
        this.xPoint = xPoint;
    }

    public int getYPoint() {
        return yPoint;
    }

    public void setYPoint(int yPoint) {
        this.yPoint = yPoint;
    }
}
