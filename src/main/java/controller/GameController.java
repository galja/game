package controller;

import messages.Messages;
import model.*;


import java.util.List;
import java.util.ListIterator;

public class GameController {
    private static final int WIDTH = 10;
    private static final int LENGTH = 10;
    private static final int NUMBER_OF_MUSHROOMS = 4;
    private static final int NUMBER_OF_POISONS = 4;
    private static final int GO_BACK = 2;
    private static final int GO_STRAIGHT = 8;
    private static final int GO_LEFT = 4;
    private static final int GO_RIGHT = 6;

    private int score;
        private MushroomListGenerator mushroomListGenerator = new MushroomListGenerator(LENGTH, WIDTH);
    private Pig pig = new Pig(LENGTH/2, WIDTH/2);
    private List<Mushroom> mushrooms;

    public GameController() {
        pig.setIsAlive(true);
        mushrooms = mushroomListGenerator.getMushrooms(NUMBER_OF_MUSHROOMS, NUMBER_OF_POISONS);
    }

    public String getGameResult(){
        if(score == NUMBER_OF_MUSHROOMS){
            return Messages.WIN.toString();
        }else{
            return Messages.DEFEAT.toString();
        }
    }

    public boolean isGameOver() {
        if ((!pig.isAlive())||(score ==NUMBER_OF_MUSHROOMS)) {
            return true;
        } else{
            return false;
        }
    }

    public void moveGirl(int direcrion) {
        switch (direcrion) {
            case GO_BACK:
                if(pig.getYPoint()==LENGTH-1){
                    pig.setYPoint(0);
                }else{
                    pig.goBack();
                }
                break;
            case GO_STRAIGHT:
                if(pig.getYPoint()==0){
                    pig.setYPoint(LENGTH-1);
                }else{
                    pig.goStraight();
                }
                break;
            case GO_LEFT:
                if(pig.getXPoint()==0){
                    pig.setXPoint(WIDTH-1);
                }else{
                    pig.goLeft();
                }
                break;
            case GO_RIGHT:
                if(pig.getXPoint()==WIDTH-1){
                    pig.setXPoint(0);
                }else{
                    pig.goRight();
                }
                break;
        }
        checkMushrooms(pig.getXPoint(), pig.getYPoint());
    }

    public void showBoard() {
        for (int i = 0; i < LENGTH; i++) {
            for (int j = 0; j < WIDTH; j++) {
                System.out.print(getSymbol(i, j));

            }
            System.out.println();
        }
    }

    private char getSymbol(int y, int x) {
        char c = '_';
        if ((pig.getYPoint() == y) && (pig.getXPoint() == x)) {
            c = pig.getAppearance();
        } else {
            for (Mushroom f : mushrooms) {
                if ((f.getYPoint() == y) && (f.getXPoint() == x)) {
                    c = f.getAppearance();
                }
            }
        }
        return c;
    }

    private void checkMushrooms(int x, int y) {
        ListIterator<Mushroom> iterator = mushrooms.listIterator();
        while (iterator.hasNext()) {
            Mushroom mushroom = iterator.next();
            if ((mushroom.getYPoint() == y) && (mushroom.getXPoint() == x)) {
                if (mushroom.isPoisonous()) {
                    pig.setIsAlive(false);
                } else {
                    score++;
                    iterator.remove();

                }
            }
        }
    }
}
