package view;

import controller.GameController;
import messages.Messages;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GameView {
    private GameController controller = new GameController();

    public void start() {
        System.out.println(Messages.RULES.toString());
        while (!controller.isGameOver()) {
            clearScreen();
            controller.showBoard();
            System.out.print(Messages.DIRECTION.toString());
            controller.moveGirl(scanAnInteger());
            System.out.println();
        }
        System.out.println(controller.getGameResult());
    }

    private int scanAnInteger() {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        try {
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println(Messages.INVALIDCHARACTERENTERED.toString());
        }
        return number;
    }

    private void clearScreen() {
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }
}
