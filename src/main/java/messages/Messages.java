package messages;

public enum Messages {
    RULES("You need to find all the healthy mushrooms (*) on the board.\n" +
            "If you pick up poisonous mushroom (+) you will die.\n" +
            "Press 2 tj go back, 8 to straight, 4 to turn left, 6 to turn right\n"),
    WIN("You won"), DEFEAT("You lost"), DIRECTION("Enter direction: "), INVALIDCHARACTERENTERED("Enter an integer, please");

    private String message;

    Messages(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
